set root_folder=%~d0%~p0

rmdir %root_folder%\gost19 /s /q
mkdir %root_folder%\gost19

del gost19.zip

"C:\Program Files\7-Zip\7z.exe" a %root_folder%\gost19\ru.iv-soft.gost19pdf.zip ru.iv-soft.gost19pdf
copy setup.bat %root_folder%\gost19\setup.bat
copy sample.ditamap %root_folder%\gost19\sample.ditamap

cd %root_folder%\gost19
"C:\Program Files\7-Zip\7z.exe" a %root_folder%\gost19.zip .

call %root_folder%\gost19\setup.bat
cd %root_folder%
