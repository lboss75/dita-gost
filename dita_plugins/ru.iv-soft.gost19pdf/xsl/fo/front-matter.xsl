<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet exclude-result-prefixes="ditaarch opentopic e dita-ot opentopic-func" version="2.0" xmlns:dita-ot="http://dita-ot.sourceforge.net/ns/201007/dita-ot" xmlns:ditaarch="http://dita.oasis-open.org/architecture/2005/" xmlns:e="http://dita.iv-soft.ru/dita/gost19" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:opentopic="http://www.idiominc.com/opentopic" xmlns:opentopic-func="http://www.idiominc.com/opentopic/exsl/function" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!--cover-->
  <xsl:template name="createFrontCoverContents">
    <xsl:variable name="prodinfo" select="/descendant::*[contains(@class, ' topic/prodinfo ')][1]"/>
    <fo:block-container xsl:use-attribute-sets="__frontmatter__approve_block">
      <fo:block >
        <xsl:call-template name="getVariable">
          <xsl:with-param name="id" select="'Approved Text'"/>
        </xsl:call-template>
      </fo:block>
      <fo:block>
        <xsl:value-of select="$prodinfo/*[contains(@class, ' topic/featnum ')]/text()" />-ЛУ
      </fo:block>
    </fo:block-container>
    <fo:block-container xsl:use-attribute-sets="__frontmatter__title">
      <fo:block>
        <xsl:value-of select="$prodinfo/*[contains(@class, ' topic/prodname ')]/text()" />
      </fo:block>
      <fo:block>
        <xsl:apply-templates select="$map/*[contains(@class,' topic/title ')][1]" />
      </fo:block>
      <fo:block>
        <xsl:value-of select="$prodinfo/*[contains(@class, ' topic/component ')]/text()" />
      </fo:block>
      <fo:block>
        <xsl:value-of select="$prodinfo/*[contains(@class, ' topic/featnum ')]/text()" />
      </fo:block>
      <fo:block>
        Листов <fo:page-number-citation-last ref-id="{$e:root-id}" />
      </fo:block>
    </fo:block-container>
  </xsl:template>
</xsl:stylesheet>
